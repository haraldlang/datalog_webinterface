Quickstart:

1) Install required modules. Simply type 'npm install'.

2) Configure directories and server-port in file 'config.json'

3) Start server: 'node datalog-server.js'